﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDownloader
{
    class Webpage
    {
        private List<string> _linksOut;
        private string _url;

        public Webpage(string url)
        {
            _url = url;
            _linksOut = new List<string>();
        }

        public string Name => _url.Replace("www.", "").Replace("http://", "").Replace("https://", "").Split('/')[0];
        public string GetUrl => _url;

        public void AddLinkOut(string link)
        {
            _linksOut.Add(link);
        }
        public void AddLinksOut(List<string> links)
        {
            _linksOut.AddRange(links);
        }
    }
}
