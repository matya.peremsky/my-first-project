﻿using MariGold.HtmlParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebDownloader
{
    class Downloader
    {
        public string Status { get; private set; }

        public Downloader()
        {
            Status = "waiting...";
        }
        public Task<string> getWebPageContent(string url)
        {
            Task<string> result = null;
            try
            {
                using (var wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    wc.DownloadProgressChanged += Wc_DownloadProgressChanged;

                    result = wc.DownloadStringTaskAsync(url);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return result;
        }

        public List<string> getUrlFromHtml(string html)
        {
            var urls = new List<string>();

            HtmlParser parser = new HtmlTextParser(html);
            var node = parser.Current;

            while (parser.Traverse())
            {
                urls.AddRange(ParseHTMLToGetURLs(parser.Current));
            }

            return urls;
        }
        private List<string> ParseHTMLToGetURLs(IHtmlNode node)
        {
            var urls = new List<string>();

            if (node.HasChildren)
            {
                foreach (var child in node.Children)
                {
                    if (child.Tag == "a" && child.Attributes.ContainsKey("href") && (child.Attributes["href"].StartsWith("https") || child.Attributes["href"].StartsWith("http")))
                    {
                        urls.Add(child.Attributes["href"]);
                    }
                    else
                    {
                        urls.AddRange(ParseHTMLToGetURLs(child));
                    }
                }
            }

            return urls;
        }


        private void Wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Status = e.ProgressPercentage + "%";
        }
    }
}
