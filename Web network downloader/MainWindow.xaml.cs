﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Web_network_downloader
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Downloader downloader;

        public MainWindow()
        {
            InitializeComponent();
            downloader = new Downloader();
        }

        private void StartBtn_Click(object sender, RoutedEventArgs e)
        {
            work();
        }
        public async void work()
        {
            try
            {
                List<string> htmls = new List<string>();
                string rootWebsiteHtml = await downloader.getWebPageContent("https://stackoverflow.com/questions/47860772/gitlab-remote-http-basic-access-denied-and-fatal-authentication/51036707");
                htmls.Add(rootWebsiteHtml);
                for (int i = 0; i < 2; i++) //how many itterations
                {
                    Console.WriteLine($"--------------------Itteration {i}----------------------");
                    List<string> links = htmlsToLinks(htmls);
                    List<Task<string>> tasks = new List<Task<string>>();
                    foreach (var link in links)
                    {
                        tasks.Add(downloader.getWebPageContent(link));
                    }
                    htmls.AddRange(await Task.WhenAll(tasks));
                }
                Console.WriteLine("END");
            }
            catch (Exception e) { Console.WriteLine(e); }
        }
    }
    public List<string> HtmlsToLinks(List<string> htmls)
    {
        List<string> links = new List<string>();
        foreach (var html in htmls)
        {
            foreach (var link in downloader.getUrlFromHtml(html))
            {
                Console.WriteLine(link);
                links.Add(link);
            }
        }
        return links;

        //string WebName = url.Replace("www.", "").Replace("http://", "").Replace("https://", "").Split('/')[0];

    }
}
