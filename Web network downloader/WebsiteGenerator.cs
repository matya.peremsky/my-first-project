﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web_network_downloader
{
    class WebsiteGenerator
    {
        private static Downloader _downloader = new Downloader();
        public static IEnumerable<Webpage> Generate(string url)
        {
            List<string> htmls = new List<string>();
            string rootWebsiteHtml = _downloader.getWebContentTask(url).GetAwaiter().GetResult();
            htmls.Add(rootWebsiteHtml);

            while (true)
            {
                List<string> links = htmlsToLinks(htmls);
                List<Task<string>> tasks = new List<Task<string>>();
                foreach (var link in links)
                {
                    yield return new Webpage(link);
                    tasks.Add(_downloader.getWebContentTask(link));
                }
                htmls = tasksToHtmls(tasks, 10);
            }
        }
        static private List<string> tasksToHtmls(List<Task<string>> tasks, int chunkSize)
        {
            List<string> result = new List<string>();
            while (tasks.Count > 0)
            {
                chunkSize = chunkSize < tasks.Count ? chunkSize : tasks.Count;
                List<Task<string>> tasksToProcess = tasks.GetRange(0, chunkSize);
                tasks.RemoveRange(0, chunkSize);
                try
                {
                    result.AddRange(Task.WhenAll(tasksToProcess).GetAwaiter().GetResult());
                }
                catch { }
            }
            return result;
        }
        static private List<string> htmlsToLinks(List<string> htmls)
        {
            List<string> links = new List<string>();
            foreach (var html in htmls)
            {
                links.AddRange(_downloader.getUrlFromHtml(html));
            }
            return links;
        }
    }
}
